package com.example.spring.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.spring.mongo.model.Article;

public interface ArticleRepository extends MongoRepository<Article, Integer> {
	
}
