package com.example.spring.mongo.ressource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.spring.mongo.model.Article;
import com.example.spring.mongo.repository.ArticleRepository;

@RestController
@CrossOrigin
public class ArticleController {
	
	@Autowired
	private ArticleRepository repository;
	
	@PostMapping("/addCat")
	public String saveArticle(@RequestBody Article article_cat) {
		repository.save(article_cat);
		return "Ajouter avec Spring";
	}
	
	@GetMapping("/findAllCats")
	public List<Article> getArticles(){
		return repository.findAll();
	}
	
	@GetMapping("/findAllCats/{id}")
	public Optional<Article> getArticle(@PathVariable int id){
		return repository.findById(id);
	}
	
	@DeleteMapping("/deleteCat/{id}")
	public String deleteArticle(@PathVariable int id) {
		repository.deleteById(id);
		return "Article Delete with Spring";
	}
	
	
}
